package catalog

import spock.lang.Specification
import spock.lang.Shared

class RepositoryImplementationBandSpec extends Specification {

	@Shared def repository = new RepositoryBandImplementation()

	def "test #deleteAll method"() {
		when:
		repository.deleteAll()

		then:
		repository.getAll().isEmpty() == true
		repository.getAll().size() == 0
	}

	def "test #save method"() {
		given:
		def band = new Band(id: 1, name: 'Opeth', genre: 'Progressive Metal')

		when:
		repository.save(band)

		then:
		repository.getAll().isEmpty() == false
		repository.getAll().size() == 1
		repository.getAll().get(0).getId() == 1
		repository.getAll().get(0).getName() == 'Opeth'
		repository.getAll().get(0).getGenre() == 'Progressive Metal'

	}

	def "test #get method"() {
		when:
		def band = repository.get(1)

		then:
		band != null
		band.getId() == 1
		band.toString() == 'Opeth - Progressive Metal'
	}

	def "test #getAll method"() {
		def band = new Band(id: 2, name: 'Unkle', genre: 'Jazz')
		repository.save(band)

		when:
		def listBands = repository.getAll()

		then:
		listBands != null
		listBands instanceof List == true
		listBands.isEmpty() == false
		listBands.size() == 2
		listBands.get(0).getId() == 1
		listBands.get(1).getId() == 2
	}

	def "test #update method"() {
		given:
		Long bandId = 2
		String newName = 'Unkle'
		String newGenre = 'Trip Hop'
		def band = new Band(id: 2, name: newName, genre: newGenre)

		when:
		repository.update(bandId, band)
		def updatedBand = repository.get(bandId)

		then:
		updatedBand.getId() == bandId
		updatedBand.getName() == newName
		updatedBand.getGenre() == newGenre
	}

	def "test #delete method"() {
		when:
		repository.delete(1)
		def listBands = repository.getAll()

		then:
		repository.getAll().size() == 1
		listBands.get(0).getId() == 2
	}
}