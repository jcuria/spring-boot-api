package catalog

import spock.lang.Specification
import spock.lang.Shared

class RepositoryImplementationMovieSpec extends Specification {

	@Shared def repository = new RepositoryMovieImplementation()

	def "test #deleteAll method"() {
		when:
		repository.deleteAll()

		then:
		repository.getAll().isEmpty() == true
		repository.getAll().size() == 0
	}

	def "test #save method"() {
		given:
		def movie = new Movie(id: 1, title: 'Scarface', director: 'Brian de Palma')

		when:
		repository.save(movie)

		then:
		repository.getAll().isEmpty() == false
		repository.getAll().size() == 1
		repository.getAll().get(0).getId() == 1
		repository.getAll().get(0).getTitle() == 'Scarface'
		repository.getAll().get(0).getDirector() == 'Brian de Palma'
	}
	
	def "test #get method"() {
		when:
		def movie = repository.get(1)

		then:
		movie != null
		movie.getId() == 1
		movie.toString() == 'Scarface - Brian de Palma'
	}

	def "test #getAll method"() {
		def movie = new Movie(id: 2, title: 'Oldboy', director: 'Quentin Tarantino')
		repository.save(movie)

		when:
		def listMovies = repository.getAll()

		then:
		listMovies != null
		listMovies instanceof List == true
		listMovies.isEmpty() == false
		listMovies.size() == 2
		listMovies.get(0).getId() == 1
		listMovies.get(1).getId() == 2
	}

	def "test #update method"() {
		given:
		Long movieId = 2
		String newTitle = 'Old boy'
		String newDirector = 'Park Chan-wook'
		def movie = new Movie(id: 2, title: newTitle, director: newDirector)

		when:
		repository.update(movieId, movie)
		def updatedMovie = repository.get(movieId)

		then:
		updatedMovie.getId() == movieId
		updatedMovie.getTitle() == newTitle
		updatedMovie.getDirector() == newDirector
	}

	def "test #delete method"() {
		when:
		repository.delete(1)
		def listMovies = repository.getAll()

		then:
		repository.getAll().size() == 1
		listMovies.get(0).getId() == 2
	}
}