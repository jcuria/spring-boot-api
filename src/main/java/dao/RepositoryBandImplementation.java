package catalog;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class RepositoryBandImplementation implements RepositoryInterface<Band> {

	Map<Long,Band> bands;

	public RepositoryBandImplementation() {
		bands = new HashMap<Long,Band>();
		populateBandRepository();
	}

	public List<Band> getAll() {
		List<Band> bandArray = new ArrayList<Band>();
		for (Band band : bands.values()) {
			bandArray.add(band);
		}
		return bandArray;
	}

	public Band get(Long id) {
		return bands.get(id);
	}

	public void save(Band band) {
		bands.put(band.getId(), band);
	}

	public void update(Long id, Band band) {
		Band bandUpdated = bands.get(id);
		if (bandUpdated != null) {
			bandUpdated.setName(band.getName());
			bandUpdated.setGenre(band.getGenre());
		}
	}

	public void delete(Long id) {
		bands.remove(id);
	}

	public void deleteAll() {
		bands.clear();
	}

	private void populateBandRepository() {
		Band band1 = new Band(1, "Porcupine Tree", "Progressive Rock");
		Band band2 = new Band(2, "The Prodigy", "Electronic");
		bands.put(band1.getId(), band1);
		bands.put(band2.getId(), band2);
	}
}