package catalog;

import java.util.List;

interface RepositoryInterface<T> {
	
	public <T> T get(Long id);

	public List<T> getAll();

	public void save(T t);

	public void update(Long id, T t);

	public void delete(Long id);

	public void deleteAll();
}