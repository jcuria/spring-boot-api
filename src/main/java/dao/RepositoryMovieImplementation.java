package catalog;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class RepositoryMovieImplementation implements RepositoryInterface<Movie> {

	Map<Long,Movie> movies;

	public RepositoryMovieImplementation() {
		movies = new HashMap<Long,Movie>();
		populateMovieRepository();
	}

	public List<Movie> getAll() {
		List<Movie> movieArray = new ArrayList<Movie>();
		for (Movie movie : movies.values()) {
			movieArray.add(movie);
		}
		return movieArray;
	}

	public Movie get(Long id) {
		return movies.get(id);
	}

	public void save(Movie movie) {
		movies.put(movie.getId(), movie);
	}

	public void update(Long id, Movie movie) {
		Movie movieUpdated = movies.get(id);
		if (movieUpdated != null) {
			movieUpdated.setTitle(movie.getTitle());
			movieUpdated.setDirector(movie.getDirector());
		}
	}

	public void delete(Long id) {
		movies.remove(id);
	}

	public void deleteAll() {
		movies.clear();
	}

	private void populateMovieRepository() {
		Movie movie1 = new Movie(1, "The Big Lebowski", "Joel Coen, Ethan Coen");
		Movie movie2 = new Movie(2, "A Clockwork Orange", "Stanley Kubrick");
		movies.put(movie1.getId(), movie1);
		movies.put(movie2.getId(), movie2);
	}
}