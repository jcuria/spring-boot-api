package catalog;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bands")
public class BandController {
	
	RepositoryInterface<Band> repository = new RepositoryBandImplementation();
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Band> getAll() {
		return repository.getAll();
	}

	@RequestMapping(method=RequestMethod.GET, value="{id}")
	public Band get(@PathVariable String id) {
		Long bandId = Long.parseLong(id);
		return repository.get(bandId);
	}

	@RequestMapping(method=RequestMethod.POST)
	public void create(@RequestBody Band band) {
		repository.save(band);
	}

	@RequestMapping(method=RequestMethod.PUT, value="{id}")
	public void update(@PathVariable String id, @RequestBody Band band) {
		Long bandId = Long.parseLong(id);
		repository.update(bandId, band);
	}

	@RequestMapping(method=RequestMethod.DELETE, value="{id}")
	public void delete(@PathVariable String id) {
		Long bandId = Long.parseLong(id);
		repository.delete(bandId);
	}

	@RequestMapping(method=RequestMethod.DELETE)
	public void deleteAll() {
		repository.deleteAll();
	}
}