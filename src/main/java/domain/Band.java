package catalog;

public class Band {

	private long id;
	private String name;
	private String genre;

	public Band() {
	}

	public Band(long id, String name, String genre) {
		this.id = id;
		this.name = name;
		this.genre = genre;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return name + " - " + genre;
	}
}