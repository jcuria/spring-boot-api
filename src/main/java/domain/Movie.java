package catalog;

public class Movie {

	private long id;
	private String title;
	private String director;

	public Movie() {
	}

	public Movie(long id, String title, String director) {
		this.id = id;
		this.title = title;
		this.director = director;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public String toString() {
		return title + " - " + director;
	}
}