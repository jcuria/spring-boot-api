# Spring Boot Api #

This is an example of a Web API using Spring Boot.

### What is used ###

* Spring Boot
* Gradle 2.12
* JDK 1.7
* Groovy 2.4
* Spock 1.0

### How to run this app ###

* Test: ./gradle clean test 
* Build: ./gradle build
* Run: java -jar build/libs/catalog-rest-service-0.1.0.jar

### Access ##
http://localhost:8080/movies
http://localhost:8080/bands